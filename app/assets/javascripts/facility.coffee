# Before You look at this code I whant to say: Sorry, it's my first difficult work, and I'm poor-experianced at JS. It should be REFACTORED!!

jQuery ->
 $(document).on "turbolinks:load", () ->
  $(".chartkick").click ->
   text = $(this).children("i").text()
   #text == "Показать графики" ? text = "Скрыть графики" : text = "Показать графики"
   $(this)
    .children(".charts").toggle()
    .children("i").text(text)
  context_menu = () ->
   $(".context-menu")
    .contextmenu ->
     $(this).children(".context-menu-list").toggle()
     $(".context-menu-list").mouseleave ->
      $(".context-menu-list").hide()
    .mouseenter ->
     menu = $(this).parent().find("li")
     menu.show(duration: 400) unless $(this).parents(".boxing").hasClass("obfuscation")
     li = $(this).children("li")
     li.mouseenter ->
      $(this).show(duration: 400)
    .mouseleave ->
     menu = $(this).parent().find("li")
     menu.hide(duration: 400)
  #-------------------------------------------------------------FUR EDIT PARTIAL
  $(".context-menu")
   .contextmenu ->
     $(this).children(".context-menu-list").toggle()
     $(".context-menu-list").mouseleave ->
      $(".context-menu-list").hide()
   .mouseenter ->
    menu = $(this).parent().find("li")
    menu.show(duration: 400) unless $(this).parents(".boxing").hasClass("obfuscation")
    li = $(this).children("li")
    li.mouseenter ->
     $(this).show(duration: 400)
   .mouseleave ->
    menu = $(this).parent().find("li")
    menu.hide(duration: 400)

  $(".num-2").on 'cocoon:after-remove', (e, object) ->
   $('.hidden').hide()
   object.parents(".num-2").children().show()
   $('.hidden').hide()
   $(this).find("#"+object.attr("id")).hide().addClass('hidden')

  $(".add-inside-object-button").click ->
   $(this).parents("table").find("#triangle").toggleClass("fa-caret-down").toggleClass("fa-caret-up")
   $(this).parents("table").find(".nested-list").show()
   $(this).parents("table").find(".inside-object-box").show()

  $(".triangle").click ->
   $(this).parent().children(".inside-object-box").toggle()
   $(this).children("#triangle").toggleClass("fa-caret-down").toggleClass("fa-caret-up")
  
  $(".num-1").children(".nested-fields").find(".input").click -> # focus do not work
   parent = $(this).parents(".nested-fields")
   boxId = parent.attr("id")
   parentComumn = $("#"+parent.attr("data-parent-id"))
   parent.parent().children().addClass("obfuscation")
   $("#"+parent.attr("id")).removeClass("obfuscation")
   $(".num-3").children(".nested-fields").hide() #fadeOut(400)
   $(".num-2").children(".nested-fields").hide()
   thisBoxId = $(this).parents(".boxing").attr("id")
   $('.num-2').find("[data-parent-id='"+thisBoxId+"']").show().removeClass("obfuscation")

  $(".num-2").children(".nested-fields").find(".input").click -> # focus do not work
   parent = $(this).parents(".nested-fields")
   parent.parent().children().addClass("obfuscation")
   $("#"+parent.attr("id")).removeClass("obfuscation")
   $(this).parents('.nested-fields').removeClass("obfuscation")
   $(this).children(".nested-list").removeClass("obfuscation")
   parentCollumn = $("#"+parent.attr("data-parent-id"))
   parentCollumn.parent().children().addClass("obfuscation")
   parentCollumn.removeClass("obfuscation")
   $(".num-3").children(".nested-fields").hide()
   thisBoxId = $(this).parents(".boxing-smaller").attr("id")
   $('.num-3').find("[data-parent-id='"+thisBoxId+"']").show().removeClass("obfuscation")

  $(".num-3").children(".nested-fields").find(".input").click -> # focus do not work
   parent = $(this).parents(".nested-fields")
   parent.parent().children().addClass("obfuscation")
   $("#"+parent.attr("id")).removeClass("obfuscation")
   parentCollumn = $("#"+parent.attr("data-parent-id"))
   grandParentCollumn = $("#"+parentCollumn.attr("data-parent-id"))
   parentCollumn.parent().children().addClass("obfuscation")
   parentCollumn.removeClass("obfuscation")
   grandParentCollumn.parent().children().addClass("obfuscation")
   grandParentCollumn.removeClass("obfuscation")

  #-------------------------------------------------------------FUR EDIT PARTIAL  
  toId = (id) ->
   $("#"+id)

  $(".num-1").on 'cocoon:after-insert', (e, object) ->
   context_menu.call()
   $(".boxing input").click ->
    thisBoxId = $(this).parents(".boxing").attr("id")
    collumn = $(this).parents(".num-1")     
    #------------------ Obfuscating for elements
    collumn.children().addClass("obfuscation")
    toId(thisBoxId).removeClass("obfuscation")
    $('.num-3').children().hide()
    $('.num-2').children().hide()
    $('.num-2').find("[data-parent-id='"+thisBoxId+"']").show()
    #------------------ Obfuscating for elements
  $(".num-2").on 'cocoon:after-insert', (e, object) ->
   context_menu.call()
   $(".boxing-smaller input").click ->
    this_box = $(this).parents(".boxing-smaller")
    thisBoxId = this_box.attr("id")
    thisBoxParentId = this_box.attr("data-parent-id")
    parent = $(this).parents(".num-2")
    #------------------ Obfuscating for elements
    $('.num-1').children().addClass("obfuscation")
    $(".num-1").find("#"+thisBoxParentId).removeClass("obfuscation")
    parent.children().addClass("obfuscation")
    toId(thisBoxId).removeClass("obfuscation")
    $('.num-3').children().hide()
    $('.num-3').find("[data-parent-id='"+thisBoxId+"']").show()
    #------------------ Obfuscating for elements
   $(".add-inside-object-button").click ->
    this_box = $(this).parents(".nested-fields")
    this_triangle = this_box.find("#triangle")
    this_box_list = this_box.find(".nested-list")
    this_box_list.show()
    this_box_list.find(".inside-object-box").show()
    this_triangle.removeClass("fa-caret-down").addClass("fa-caret-up") if this_triangle.hasClass('fa-caret-down')
    #------------------ Hiding and Showing Nested List Elements
    this_triangle.click ->
     if this_triangle.hasClass("fa-caret-down")
      $(this).removeClass("fa-caret-down").addClass("fa-caret-up")
      this_box_list.find(".inside-object-box").show()
     else
      $(this).addClass("fa-caret-down").removeClass("fa-caret-up")
      this_box_list.find(".inside-object-box").hide()

    #------------------ Hiding and Showing Nested List Elements
  $(".num-3").on 'cocoon:after-insert', (e, object) ->
   context_menu.call()
   $(".boxing-smallest input").click ->
    this_box = $(this).parents(".boxing-smallest")
    thisBoxId = this_box.attr("id")
    thisBoxParentId = this_box.attr("data-parent-id")
    thisBoxGrandParentId = $(".num-2").find("#"+thisBoxParentId).attr("data-parent-id")
    parent = $(this).parents(".num-3")
    # NEED FIX ------------------
    #------------------ Obfuscating for elements
    $('.num-2').children().addClass("obfuscation")
    $(".num-2").find("#"+thisBoxParentId).removeClass("obfuscation")
    $('.num-1').children().addClass("obfuscation")
    $(".num-1").find("#"+thisBoxGrandParentId).removeClass("obfuscation")
    parent.children().addClass("obfuscation")
    toId(thisBoxId).removeClass("obfuscation")
    #------------------ Obfuscating for elements
   $(".add-inside-object-button").click ->
    this_box = $(this).parents(".nested-fields")
    this_triangle = this_box.find("#triangle")
    this_box_list = this_box.find(".nested-list")
    this_box_list.show()
    this_box_list.find(".inside-object-box").show()
    #------------------ Hiding and Showing Nested List Elements
    this_triangle.removeClass("fa-caret-down").addClass("fa-caret-up") if this_triangle.hasClass('fa-caret-down')
    this_triangle.click ->
     if this_triangle.hasClass("fa-caret-down")
      $(this).removeClass("fa-caret-down").addClass("fa-caret-up")
      this_box_list.find(".inside-object-box").show()
     else
      $(this).addClass("fa-caret-down").removeClass("fa-caret-up")
      this_box_list.find(".inside-object-box").hide()
    #------------------ Hiding and Showing Nested List Elements
  boxingOrderNumber = 1
  smallerBoxingOrderNumber = 1
  smallestBoxingOrderNumber = 1
  #---------------------------------------------------- Adding columns ------------------ NUM-1
  $(".num-1").on 'cocoon:before-insert', (e, object) ->
   context_menu.call()
   uniq_id = new Date().getTime()
   object
    .attr("id", uniq_id)
   lastInList = $(this).children().last().find('p').text()
   boxingOrderNumber = Number(lastInList) + 1
   object
    .attr("data-order-number", boxingOrderNumber)
    .find("p").text(boxingOrderNumber)
   #---------------------------------------------------- Adding columns ------------------ NUM-2
   link = object.find(".add-field-link-be")
   link.click ->
    $(".num-2").on 'cocoon:after-insert', (e, nested_object) ->
     context_menu.call()
     if true#boxingOrderNumber+"" == object.attr("data-order-number")
      uniq_id_2 = new Date().getTime()
      nested_object
       .attr("data-parent-id", uniq_id)
       .attr("id", uniq_id_2)
      lastInSmallerList = $(this).find("[data-parent-id='"+uniq_id+"']").last().find('p').text()
      #smallerRegEx = /[\d]+/.exec(/[\.][\d]+/.exec(lastInSmallerList))
      smallerRegEx = /[\d]+$/.exec(lastInSmallerList)
      smallerBoxingOrderNumber = Number(smallerRegEx) + 1
      nested_object.attr("data-order-number", smallerBoxingOrderNumber)
      parentOrderNumber = $("#"+uniq_id).find('p').text()
      nested_object.find("p").text(parentOrderNumber+"."+smallerBoxingOrderNumber)

      nested_object.find(".add-inside-object-button")
       .attr("data-association-insertion-node", "#" + uniq_id_2 + " .inside-object-box")
       .attr("id", uniq_id_2)
      #---------------------------------------------------- Adding columns ------------------ NUM-2 inside-object
      $(".add-inside-object-button").click ->
       linkId = $(this).attr("id")
       $(".inside-object-box-2").on 'cocoon:before-insert', (e, inside_object_object) ->
        uniq_id_3 = new Date().getTime()
        inside_object_object
          .attr("data-inside-object-parent-id", linkId)
          .attr("id", uniq_id_3)
      #---------------------------------------------------- Adding columns ------------------ NUM-3
     link = nested_object.find(".add-field-link-pl")
     link.click ->
      $(".num-3").on 'cocoon:after-insert', (e, super_nested_object) ->
       context_menu.call()
       if true#smallerBoxingOrderNumber+"" == nested_object.attr("data-order-number")
        uniq_id_3 = new Date().getTime()
        parentOrderNumber = $("#"+uniq_id_2).find("p").text()
        super_nested_object
         .attr("data-parent-id", uniq_id_2)
         .attr("id", uniq_id_3)
        lastInSmallestList = $(this).find("[data-parent-id='"+uniq_id_2+"']").last().find('p').text() # "54.35.160"
        forthStep = /[\d]+$/.exec(lastInSmallestList)
        smallestBoxingOrderNumber = Number(forthStep) + 1
        super_nested_object
         .attr("data-order-number", smallestBoxingOrderNumber)
         .find("p").text(parentOrderNumber+"."+smallestBoxingOrderNumber)
         .find(".add-field-link").attr("id", uniq_id_3)
        super_nested_object.find(".add-inside-object-button").attr("data-association-insertion-node", "#" + uniq_id_3 + " .inside-object-box")
        #---------------------------------------------------- Adding columns ------------------ NUM-3 inside-object
        $(".add-inside-object-button").click ->
         $(".inside-object-box-3").on 'cocoon:after-insert', (e, inside_object_object) ->
          context_menu.call()
          uniq_id_4 = new Date().getTime()
          inside_object_object
           .attr("data-inside-object-parent-id", uniq_id_3)
           .attr("id", uniq_id_4)
           .parents(".nested-list").show()
   #---------------------------------------------------- Adding columns






  $(".add-field-link-be").click ->
   uniq_id = $(this).parents(".nested-fields").attr("id")
   $(".num-2").on 'cocoon:before-insert', (e, nested_object) ->
    context_menu.call()
    uniq_id_2 = new Date().getTime()
    nested_object
     .attr("data-parent-id", uniq_id)
     .attr("id", uniq_id_2)
    lastInSmallerList = $(this).find("[data-parent-id='"+uniq_id+"']").last().find('p').text()
    smallerRegEx = /[\d]+/.exec(/[\.][\d]+/.exec(lastInSmallerList))
    smallerBoxingOrderNumber = Number(smallerRegEx) + 1
    nested_object.attr("data-order-number", smallerBoxingOrderNumber)
    parentOrderNumber = $("#"+uniq_id).find('p').text()
    nested_object.find("p").text(parentOrderNumber+"."+smallerBoxingOrderNumber)
    $("#"+nested_object.attr("id")).find(".add-inside-object-button").attr("data-association-insertion-node", "#" + uniq_id_2 + " .inside-object-box")
    nested_object.find(".add-field-link").attr("id", uniq_id_2)
    #---------------------------------------------------- Adding columns ------------------ NUM-2 inside-object
    $(".inside-object-box-2").on 'cocoon:after-insert', (e, inside_object_object) ->
     nested_object.find(".add-inside-object-button").attr("data-association-insertion-node", "#" + uniq_id_2 + " .inside-object-box")
     uniq_id_3 = new Date().getTime()
     inside_object_object
      .attr("data-parent-id", uniq_id_2)
      .attr("id", uniq_id_3)
    #---------------------------------------------------- Adding columns ------------------ NUM-3
  $(".add-field-link-pl").click ->
   uniq_id_2 = $(this).parents(".nested-fields").attr("id")
   $(".num-3").on 'cocoon:before-insert', (e, super_nested_object) ->  
    context_menu.call()
    uniq_id_3 = new Date().getTime()
    parentOrderNumber = $("#"+uniq_id_2).find("p").text()
    super_nested_object
     .attr("data-parent-id", uniq_id_2)
     .attr("data-inside-object-parent-id", uniq_id_2)
     .attr("id", uniq_id_3)
    lastInSmallestList = $(this).find("[data-parent-id='"+uniq_id_2+"']").last().find('p').text() # "54.35.160"
    firstStep = /[\.][\d][\.][\d]/.exec(lastInSmallestList) # ".35.160"
    secondStep = /[\d]+[\.]+[\d]+/.exec(firstStep) # "35.160"
    thirdStep = /[\.][\d]+/.exec(secondStep) # ".160"
    forthStep = /[\d]/.exec(thirdStep) # "160"
    smallestBoxingOrderNumber = Number(forthStep) + 1
    super_nested_object
     .attr("data-order-number", smallestBoxingOrderNumber)
     .find("p").text(parentOrderNumber+"."+smallestBoxingOrderNumber)
     #.find(".add-field-link").attr("id", uniq_id_3)
     .find(".add-inside-object-button").attr("data-association-insertion-node", "#" + uniq_id_3 + " .inside-object-box")
    #---------------------------------------------------- Adding columns ------------------ NUM-3 inside-object
    $(".inside-object-box-3").on 'cocoon:after-insert', (e, inside_object_object) ->
     context_menu.call()
     uniq_id_4 = new Date().getTime()
     inside_object_object
      .attr("data-inside-object-parent-id", uniq_id_3)
      .attr("data-parent-id", uniq_id_3)
      .attr("id", uniq_id_4)
      .parents(".nested-list").show()










