jQuery ->
 $(document).ready ->
   $(".user-status-button").parent().bind 'ajax:success', (e, data, status, xhr) ->
     json_respond = JSON.parse(xhr.responseText)
     button = $(this).children(".user-status-button") 
     hidden = $(this).children("#user_inspector")
     status = $(this).parents("tr").children(".status")
     if json_respond.inspector is true
       hidden.val(false)
       status.text("Инспектор")
       button.val("hi") # don't work
     else
       hidden.val(true)
       status.text("Обычный пользователь")
       button.val("hello") # don't work