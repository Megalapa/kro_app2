class Api::V1::SessionsController < Api::V1::BaseController
  def create
    @user = User.find_by(email: params[:user][:email])

    if @user && @user.valid_password?(params[:user][:password])
       @user.authenticate_via_api
       render json: @user, serializer: UserSerializer
    else
       render json: "User not found".to_json, status: :not_found
    end

  end

  private

    def user_params
      params.require(:user).permit(:email, :password)
    end
end
# user[email]=mail@mail.com&user[password]=123456