class Api::V1::UsersController < Api::V1::BaseController
  
  def create
    @user = User.new(user_params)
    if @user.save
      @user
    else
      @user.errors.full_messages
    end
  end
  
  private

    def user_params
      params.require(:user).permit(:email, :name, :password, :password_confirmation)
    end
end