class ApplicationController < ActionController::Base
  include Pundit
  # before_action :authenticate_user!
  before_action :skip_pundit, if: :devise_controller?
  before_action :configure_permitted_parameters, if: :devise_controller?
  
  after_action :verify_authorized
  
  protect_from_forgery with: :exception
  
  

  def skip_pundit
    skip_authorization
  end

  rescue_from Pundit::NotAuthorizedError do |exception|
    redirect_to user_path(current_user), alert: exception.message
  end
  
  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
  end

  # def configure_sign_up_params
  #   devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
  # end
  # def after_sign_in_path_for(resource_or_scope)
  #   redirect_to user_path(resource_or_scope)
  # end

  # def after_sign_up_path_for(resource)
  #   after_sign_in_path_for(resource)
  # end

end