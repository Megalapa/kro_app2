class EmployeesController < ApplicationController
  before_action :authenticate_user!
  before_action :facility_load, only: [:index, :new, :create, :destroy]
  before_action :load_employee, only: [:edit, :update, :destroy, :show]
  
  def index
    @employees = @facility.employees
    authorize @employees
  end

  def new
    @employee	= @facility.employees.new
    authorize @employee, :new?
  end

  def create
    @employee = @facility.employees.new employee_params
    authorize @employee, :create?
    if @employee.save
      redirect_to  facility_path(@facility)
    else
      # err_any?(@this_new)
      render :new
    end
  end

  def show
  end

  def edit
  end

  def update
  end

  def destroy
    redirect_to facility_employee_index_path(@facility) if @employee.destroy
  end

  private

    def facility_load
      @facility = Facility.find params[:facility_id]
    end

    def load_employee
      @employee = Employee.find params[:id]
      authorize @employee 
    end

    def employee_params
      params.require(:employee).permit(:name, :sex, languages: [])    
    end
end
