class ExaminationsController < ApplicationController
  before_action :authenticate_user!
  before_action :examination_load, only: [:show]
  before_action :facility_load, only: [:new, :create]

  def new
    @examination = @facility.examinations.new
    @build_elements = @facility.build_elements.includes(:placements)
    authorize @examination, :new?
  end

  def create
    @examination = @facility.examinations.new(examination_params)
    authorize @examination
    if @examination.save
      redirect_to facility_path(@facility)
    else
      puts @examination.errors.full_messages
      render :new
    end
  end

  def show
  end

  private

  def examination_load
    @examination = Examination.find(params[:id])
    @facility = @examination.facility
    authorize @examination
  end

  def facility_load
    @facility = Facility.find(params[:facility_id])
  end

  def examination_params
    params.require(:examination).permit(
                                        :user_id,
                                        unexamined: [],
                                        placements: [],
                                        nested_placements: [],
                                        inside_objects: [],
                                        comments_attributes: [:content, :parent_id, :parent_type],
                                        attachments_attributes: [file:[]]
                                        )
  end
end
