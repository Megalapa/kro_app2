class FacilityController < ApplicationController
  before_action :authenticate_user!
  before_action :facility_load, only: [:show, :edit, :update, :destroy]

  def index
    @facilities = Facility.includes(:examinations)
    authorize @facilities
  end

  def new
    @facility = Facility.new
    authorize @facility
  end

  def create
    @facility = Facility.new(facility_params)
    authorize @facility
    if @facility.save
      redirect_to facility_index_path
    else
      render :new
    end
  end

  def show
    @exams = @facility.examinations.from_year
  end

  def edit
  end

  def update
    if @facility.update(facility_params)
      redirect_to @facility
    else
      render :edit
    end
  end

  def destroy
    redirect_to root_path if @facility.destroy
  end

  def dublicating
    @facility = Facility.find params[:facility]
    authorize @facility
    new_name = params[:dublicate_name]
    @facility.dublicate(new_name)
    redirect_to root_path
  end

  private

  def facility_load
    @facility = Facility.find(params[:id])
    authorize @facility
  end

  def facility_params
    params.require(:facility).permit(
                                      :name,
                                      :total_square, 
                                      build_elements_attributes: [
                                                                   :kind, 
                                                                   :id,
                                                                   :_destroy,
                                                                   placements_attributes: [
                                                                                            :name,
                                                                                            :kind,
                                                                                            :id,
                                                                                            :_destroy,
                                                                                            nested_placements_attributes: [
                                                                                                                           :name,
                                                                                                                           :kind,
                                                                                                                           :id,
                                                                                                                           :_destroy,
                                                                                                                           furniture_attributes: [
                                                                                                                                                  :material,
                                                                                                                                                  :id,
                                                                                                                                                  :_destroy,
                                                                                                                                                  :kind
                                                                                                                                                 ],
                                                                                                                            glass_attributes: [
                                                                                                                                               :kind,
                                                                                                                                               :id,
                                                                                                                                               :_destroy,
                                                                                                                                               :square
                                                                                                                                              ],
                                                                                                                            inside_objects_attributes: [
                                                                                                                                                        :kind,
                                                                                                                                                        :id,
                                                                                                                                                        :_destroy,
                                                                                                                                                        :square
                                                                                                                                                       ]
                                                                                                                          ],
                                                                                            furniture_attributes: [
                                                                                                                   :material,
                                                                                                                   :id,
                                                                                                                   :_destroy,
                                                                                                                   :kind
                                                                                                                  ],
                                                                                            glass_attributes: [
                                                                                                               :kind,
                                                                                                               :id,
                                                                                                               :_destroy,
                                                                                                               :square
                                                                                                              ],
                                                                                            inside_objects_attributes: [
                                                                                                               :kind,
                                                                                                               :id,
                                                                                                               :_destroy,
                                                                                                               :square
                                                                                                              ]
                                                                                          ]
                                                                  ]
                                      )

  end

end
