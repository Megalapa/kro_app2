class ItinerariesController < ApplicationController
  before_action :authenticate_user!
  before_action :facility_load, only: [:index, :new, :create, :destroy]
  before_action :itinerary_load, only: [:edit, :update, :destroy]
  
  def index
    @itineraries = @facility.itineraries
    authorize @itineraries
  end
  
  def new
    @itinerary = @facility.itineraries.new()
    authorize @itinerary, :new?
  end

  def create
    @itinerary = @facility.itineraries.new(itinerary_params)
    authorize @itinerary, :create?
    if @itinerary.save
     redirect_to facility_path(@facility), notice: "Маршрутный лист создан"
    else
      render :new#, flash[:error] = @itinerary.errors.full_messages
    end
  end

  def edit
  end

  def update
  end

  def destroy
    flash[:notice] = "Маршрутный лист удален" if @itinenrary.destroy
  end

  private

    def itinerary_load
      @itinenrary = Itinerary.find(params[:id])
      authorize @itinenrary
    end

    def facility_load
      @facility = Facility.find(params[:facility_id])
    end

    def itinerary_params
      params.require(:itinerary).permit(
 	                                      :name, 
 	                                      build_elements: [], 
 	                                      placements: [], 
 	                                      nested_placements: []
      	                               )	
    end
end
