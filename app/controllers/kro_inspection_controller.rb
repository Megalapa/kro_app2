class KroInspectionController < ApplicationController
  before_action :authenticate_user!
  before_action :inspection_load, only: [:show]
  before_action :facility_load, only: [:new, :create, :show]

  def new
    @kro_inspection = @facility.kro_inspections.new
    @build_elements = @facility.build_elements.includes(:placements)
    authorize @kro_inspection, :new?
  end

  def create
    @kro_inspection = @facility.kro_inspections.new(kro_inspection_params)
    @kro_inspection.user = current_user
    authorize @kro_inspection
    if @kro_inspection.save
      redirect_to facility_path(@facility)
    else
      render :new
    end
  end

  def show
  end

  private

  def inspection_load
    @inspection = KroInspection.find(params[:id])
    authorize @inspection
  end

  def facility_load
    @facility = Facility.find(params[:facility_id])
  end

  def kro_inspection_params
    params.require(:kro_inspection).permit(placements: [], nested_placements: [], furnitures: [], glass:[])
  end
end
