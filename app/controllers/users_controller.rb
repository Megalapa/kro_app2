class UsersController < ApplicationController
  before_action :authenticate_user!
  before_action :user_load, only: [:show, :update]
  
  def index
    @users = User.all
    authorize @users
  end
  
  def show
    # RemovingGlassJob.perform_now
    # RemovingFurnitureJob.perform_now
    # ChangeInspectionNameJob.perform_now
    skip_authorization
    # skip_authorization if current_user
  end

  def update
    skip_authorization if current_user.supervisor?
    @this_user.update(users_params)
    respond_to do |format|
      format.json { render json: @this_user }
    end
  end

  private
  
  def user_load
    @this_user = User.find params[:id]
  end

  def users_params
    params.require(:user).permit(:email, :password, :name, :inspector)
  end

end