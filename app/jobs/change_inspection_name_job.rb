class ChangeInspectionNameJob < ApplicationJob
  queue_as :default

  def perform
    KroInspection.move_to_examination
  end
end
