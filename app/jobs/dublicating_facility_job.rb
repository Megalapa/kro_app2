class DublicatingFacilityJob < ApplicationJob
  queue_as :default
  # OPTIMIZE with monads or decompose logic between models each of them must be able to dublicate by them selves

  def perform(id, new_facility_name)
    Facility.transaction do
      @facilty = Facility.find id
      @new_facility = Facility.create(name: new_facility_name)

      @facilty.build_elements.each do |b_elem|
        new_attr = b_elem.attributes
        new_attr.delete("id")
        @new_b_elem = @new_facility.build_elements.create!(new_attr)
        b_elem.placements.each do |place|
          new_attr = place.attributes
          new_attr.delete("id")
          @new_place = @new_b_elem.placements.create!(new_attr)

          place.furniture.each do |fur|
            new_attr = fur.attributes
            new_attr.delete("id")
            @new_place.furniture.create!(new_attr)
          end
          place.inside_objects.each do |fur|
            new_attr = fur.attributes
            new_attr.delete("id")
            @new_place.furniture.create!(new_attr)
          end
          place.nested_placements.each do |np|
            new_attr = np.attributes
            new_attr.delete("id")
            @new_n_place = @new_place.nested_placements.create!(new_attr)

            np.furniture.each do |nfur|
              new_attr = nfur.attributes
              new_attr.delete("id")
              @new_n_place.furniture.create!(new_attr)
            end
            np.inside_objects.each do |nfur|
              new_attr = nfur.attributes
              new_attr.delete("id")
              @new_n_place.furniture.create!(new_attr)
            end
          end
        end
      end
      @new_facility
    end
  end

end
