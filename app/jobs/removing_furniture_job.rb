class RemovingFurnitureJob < ApplicationJob
  queue_as :default

  def perform
    Furniture.removing_furniture
  end
end
