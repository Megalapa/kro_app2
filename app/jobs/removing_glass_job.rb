class RemovingGlassJob < ApplicationJob
  queue_as :default

  def perform
    Glass.removing_glass
  end
end
