class Attachment < ApplicationRecord
  belongs_to :user
  belongs_to :attachable, polymorphic: true
  belongs_to :attachable_for, polymorphic: true
  mount_uploader :file, PhotoUploaderUploader
end
