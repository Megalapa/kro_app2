class BuildElement < ApplicationRecord

  belongs_to :facility
  has_many :placements, dependent: :destroy

  accepts_nested_attributes_for :placements, allow_destroy: true, reject_if: :all_blank

end
