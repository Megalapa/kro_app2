class Comment < ApplicationRecord  
  belongs_to :examination
  belongs_to :parent, polymorphic: true
  validates :content, presence: true
end
