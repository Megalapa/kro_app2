module AssociationsListing
  extend ActiveSupport::Concern
  included do
    def associations_list
      self.class.reflect_on_all_associations(:has_many).map(&:name)
    end
    class << self
      def associations_list
        reflect_on_all_associations(:has_many).map(&:name)
      end
    end
  end
end