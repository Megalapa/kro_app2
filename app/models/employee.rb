class Employee < ApplicationRecord
  belongs_to :facility
  has_many :inventory
  # has_many :sectors
  # validates :name, :surname, :birthday, :sex, :document_type, :document_number, presence: true
end
# class User < ActiveRecord::Base
#   has_many :players, :class_name => "User",  :foreign_key => "coach_id"
#   belongs_to :coach, :class_name => "User"
# end