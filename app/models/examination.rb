class Examination < ApplicationRecord
  include Attaching
  belongs_to :facility
  belongs_to :user
  has_many :comments, dependent: :destroy
  # has_many :attachments, dependent: :destroy

  # mount_uploader :photo, ExaminationShotsUploader
  # mount_uploader :photos, ExaminationShotsUploader
  
  accepts_nested_attributes_for :comments, allow_destroy: true, reject_if: proc { |attributes| attributes['content'].blank? }
  
  validates :user, presence: true

  scope :last_five, -> { order(created_at: :desc).limit(5) }
  scope :desc, -> {order(created_at: :desc)}
  scope :from_month, -> { where(created_at: DateTime.now.beginning_of_month..DateTime.now.end_of_month) }
  scope :from_year, -> { where(created_at: DateTime.now.beginning_of_year..DateTime.now.end_of_year) }
  
  before_create :total_amount_of_objects, :examinated_amount
  after_create :total_inspections, :mark_count

  class << self
    
    # TODO finish method
    
    # def unexamined
    # end

    def realness_month_data
      @data = self.from_month.pluck(:created_at, :amount_examined_objects, :total_points).map{|e| [e[0], ((e[1].to_f/e[2].to_f)*100).round(1)]}
    end

    def month_data_for_chart
      @data = self.from_month.pluck(:created_at, :mark)
    end
  
    def year_data_for_chart
      @data = self.from_year.group_by_month(:created_at).average(:mark).map{|i| [i[0],i[1].round]}
    end
  
    def last_mark 
      last ? last.mark : 0
    end
  end

  def realness #reliability      
    ((amount_examined_objects.to_f/total_points.to_f)*100.to_f).round(1)
  end


  private

    def examinated_amount
      self.amount_examined_objects = total_points - unexamined.map(&:to_i).sum
    end

    def total_inspections
      facility.update(inspections_amount: facility.examinations.count)
    end
    # OPTIMIZE can be optimized with monads
    def total_amount_of_objects
      @be = facility.build_elements.includes(:placements)
      @be.each do |b_e|
        self.total_points += b_e.placements.count
        b_e.placements.includes(:inside_objects, :nested_placements).each do |i2|
          self.total_points += i2.inside_objects.count
          self.total_points += i2.nested_placements.count
          i2.nested_placements.includes(:inside_objects).each do |i3|
            self.total_points += i3.inside_objects.count
          end
        end
      end
      self.total_points
    end
    
    def mark_count
      self.mark = 0
      [placements, nested_placements, inside_objects].each{ |i| self.mark += i.count }
      self.mark = 100.to_f - ((mark.to_f / amount_examined_objects) * 100.to_f).round(1)
      self.save
      mark
    end
end
