class Facility < ApplicationRecord
  include AbstractController::Translation

  [:documents, :build_elements, :itineraries, :employees, :inventory, :examinations, :shipments].each do |model|
    has_many model, dependent: :destroy
  end

  validates :name, presence: true
  
  accepts_nested_attributes_for :build_elements, allow_destroy: true, reject_if: :all_blank

  def last_mark
    examinations.last ? examinations.last.mark : 0
  end

  # def self.associations_list
  #   reflect_on_all_associations(:has_many).map(&:name)
  # end

  # def associations_list
  #   self.class.reflect_on_all_associations(:has_many).map(&:name)
  # end

  def amout_of_all_inspections
    examinations ? examinations.count : 0
  end

  def data_for_chart
    data = examinations.pluck(:created_at, :mark).map{|i| ["#{i.first.asctime}", i.second ]}
  end

  def dublicate(new_facility_name)
    DublicatingFacilityJob.perform_now(self.id, new_facility_name)
  end

end