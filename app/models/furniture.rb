class Furniture < ApplicationRecord
  belongs_to :placement

  def self.removing_furniture
    
    self.all.each do |g|
      g.placement.inside_objects.create(kind: g.kind)
    end
    self.destroy_all
  end

end
