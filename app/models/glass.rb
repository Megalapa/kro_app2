class Glass < ApplicationRecord
  belongs_to :placement

  def self.removing_glass
    
    self.all.each do |g|
      g.placement.furniture.create(kind: g.kind)
    end
    self.destroy_all
  end

end
