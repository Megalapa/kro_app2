class Inventory < ApplicationRecord
  belongs_to :facility
  belongs_to :employee
end
