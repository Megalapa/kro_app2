class KroInspection < ApplicationRecord
  belongs_to :facility
  belongs_to :user
  
  validates :user, presence: true

  scope :last_five, -> { order(created_at: :desc).limit(5) }
  scope :desc, -> {order(created_at: :desc)}
  scope :from_month, -> { where(created_at: DateTime.now.beginning_of_month..DateTime.now.end_of_month) }

  before_create :total_available_count, :mark_count
  after_create :total_inspections

  def self.move_to_examination
    KroInspection.transaction do
      self.all.each do |inspection|
        inspection.facility.examinations.create!(
                                                 placements: inspection.placements,
                                                 nested_placements: inspection.nested_placements,
                                                 inside_objects: inspection.furnitures,
                                                 inside_objects: inspection.glass,
                                                 user: inspection.user
                                                )
      end
      self.destroy_all
    end
  end

  def self.data_for_chart
    data = self.from_month.pluck(:created_at, :mark)
  end

  def self.last_mark 
    last ? last.mark : 0
  end

  private

    def total_inspections
      facility.update(inspections_amount: facility.kro_inspections.count)
    end
    # OPTIMIZE can be optimized with monads
    def total_available_count
      @be = facility.build_elements.includes(:placements)
      @be.each do |b_e|
        self.total_points += b_e.placements.count
        b_e.placements.includes(:furniture, :nested_placements).each do |i2|
          self.total_points += i2.furniture.count
          self.total_points += i2.nested_placements.count
          i2.nested_placements.includes(:furniture).each do |i3|
            self.total_points += i3.furniture.count
          end
        end
      end
    end
    
    def mark_count
      [placements, nested_placements, furnitures, glass].each{ |i| self.mark += i.count }
      self.mark = (mark * 100.to_f / total_points).round(1)
    end
  
end