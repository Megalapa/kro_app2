class Placement < ApplicationRecord

  belongs_to :build_element
  has_many :inside_objects, dependent: :destroy
  has_many :nested_placements, class_name: 'Placement', foreign_key: 'placement_id'

  has_many :furniture, dependent: :destroy
  has_many :glass, dependent: :destroy

  accepts_nested_attributes_for :glass, :furniture, :inside_objects, :nested_placements, allow_destroy: true, reject_if: :all_blank

end