class Supply < ApplicationRecord
  belongs_to :shipment
  validates :shipment_id, presence: true
end
