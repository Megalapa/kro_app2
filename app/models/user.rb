class User < ApplicationRecord
  has_many :examinations
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable

  validates :name, presence: true

   def role
     supervisor? || inspector?
   end

   def authenticate_via_api
     self.token_for_api = gen_token
     self.api_token_expires_at = 2.hour.from_now
     save
   end

   def gen_token
     @token = SecureRandom.base64(18).delete('/')
   end
end
