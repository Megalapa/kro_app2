class EmployeePolicy < ApplicationPolicy
  def index?
    user.role
  end

  def show?
    user.role
  end

  def new?
    create?
  end

  def create?
    user.role
  end

  def update?
    user.role
  end

  def destroy?
    user.role
  end
  class Scope < Scope
    def resolve
      scope
    end
  end
end
