class ExaminationPolicy < ApplicationPolicy
  def create?
    user.role
  end

  def show?
    true
  end
  class Scope < Scope
    def resolve
      scope
    end
  end
end
