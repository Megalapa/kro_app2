class FacilityPolicy < ApplicationPolicy
  
  class Scope < Scope
    
    def resolve
      if user.role
        scope.all
      end
    end

  end
  
  def index?
    user.role
    # true
  end

  def create?
    user.role
  end

  def show?
    true
  end

  def update?
    user.role
  end

  def destroy?
    user.role
  end

  def dublicating?
    user.role
  end

end