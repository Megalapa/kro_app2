class ItineraryPolicy < ApplicationPolicy
  def index?
    user.role
  end

  def create?
    user.role
  end

  def update?
    user.role
  end

  def destroy?
    user.role
  end
  class Scope < Scope
    def resolve
      scope
    end
  end
end
