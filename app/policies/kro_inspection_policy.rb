class KroInspectionPolicy < ApplicationPolicy

  def create?
    user.role
  end

  def show?
    true
  end

  def destroy?
    user.role
  end
end