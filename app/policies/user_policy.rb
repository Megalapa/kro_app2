class UserPolicy < ApplicationPolicy
  
  def index?
    user.supervisor
  end

  def show?
    true
  end

  def update?
    user.supervisor
  end

  class Scope < Scope
    def resolve
      scope
    end
  end
end
