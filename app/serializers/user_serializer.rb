class UserSerializer < ActiveModel::Serializer
  
  attributes :id, :email, :supervisor, 
             :inspector, :name, :token_for_api, :api_token_expires_at
  
  has_many :examinations

  def examinations_id
    object.id if object.persisted?
  end
end