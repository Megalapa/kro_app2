class PhotoUploaderUploader < CarrierWave::Uploader::Base
  CarrierWave::SanitizedFile.sanitize_regexp = /[^[:word:]\.\-\+]/
  # delegate :identifier, to: :file
  # Include RMagick or MiniMagick support:
  # include CarrierWave::RMagick
  # include CarrierWave::MiniMagick

  storage :file

  # def store_dir
  #   "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  # end

  def store_dir
    "examinations_photos/#{model.class.to_s.underscore}/#{mounted_as}/#{model.attachable_type}_#{model.attachable_id}/#{model.attachable_for_type}_#{model.attachable_for_id}"
  end

end
