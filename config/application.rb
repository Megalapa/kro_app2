require_relative 'boot'

# require 'rails/all'

require "active_model/railtie"
require "active_job/railtie"
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "action_cable/engine"
require "sprockets/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)
# I18n.default_locale = :ru
# I18n.locale = :ru

module KroApp2
  class Application < Rails::Application
    # Allowing cross domain requests CORS 
    config.action_dispatch.default_headers.merge!({
      'Access-Control-Allow-Origin' => '*',
      'Access-Control-Request-Method' => '*'
    })

  	config.assets.paths << Rails.root.join('app', 'assets', 'fonts')
    config.assets.compile = true
    # config.assets.precompile =  ['*.js', '*.css', '*.css.erb'] 
    config.assets.precompile += %w( .svg .eot .woff .ttf .js .css )
    config.active_record.default_timezone = :utc
    config.time_zone = 'Moscow' # set default time zone to "Moscow" (UTC +4)
    config.i18n.default_locale = :ru # set default locale to Russian
    config.i18n.available_locales = [:ru, :en]
    # config.active_record.raise_in_transactional_callbacks = true
    config.action_cable.disable_request_forgery_protection = false
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    config.active_job.queue_adapter = :sidekiq
    config.generators do |g|
      g.test_framework :rspec,
                   fixtures: true,
                   view_spec: false,
                   helper_specs: false,
                   routing_specs: false,
                   request_specs: false,
                   controller_spec: true
      g.fixture_replacement :factory_girl, dir: 'spec/factories'
    end
    # Do not swallow errors in after_commit/after_rollback callbacks.
    # config.active_record.raise_in_transactional_callbacks = true
  end
end
