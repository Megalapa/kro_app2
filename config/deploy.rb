# config valid only for current version of Capistrano
lock '3.10.0'

set :application, 'e_check_list'
set :repo_url, 'git@bitbucket.org:Megalapa/kro_app2.git'

# # Default branch is :master
# # ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# # Default deploy_to directory is /var/www/my_app_name
    set :deploy_to, '/home/deployer/app'

#     set :bundle_flags, '--verbose'

    set :ssh_options, compression: false, keepalive: true

    set :linked_dirs, fetch(:linked_dirs, []).push('log',
                                               'tmp/pids',
                                               'tmp/cache',
                                               'tmp/sockets',
                                               'vendor/bundle',
                                               'public/system',
                                               '_public/uploads ')

    set :keep_releases, 3

namespace :deploy do
  COMMANDS = %w(start stop restart)

  COMMANDS.each do |command|
    task command do
      on roles(:app), in: :sequence, wait: 5 do
        within current_path do
          execute :bundle, "exec thin #{command} -C config/thin.yml"
        end
      end
    end
  end

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end
end