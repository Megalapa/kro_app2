require 'sidekiq/web'
Rails.application.routes.draw do

  authenticate :user, -> (user) { user.supervisor? } do
    # No route 'GET /sidekiq'
    # TODO root_path unless user.admin?
    mount Sidekiq::Web => '/sidekiq'
  end
  
  devise_for :users

  
  devise_scope :user do
    get 'sign_in', to: 'devise/sessions#new'
  end
  
  # root to: {controller:"devise/sessions", action:"new"}
  root 'facility#index'
  post '/dublicate' => 'facility#dublicating'

  concern :commentable do
    resources :comments, only: [:create, :destroy]
  end
  
  resources :users, only: [:show, :edit, :update, :index]
  resources :facility, shallow: true do
    resources :kro_inspection, only: [:new, :create, :show]
    resources :examinations, concerns: [:commentable], only: [:new, :create, :show] do
      # resources :comments, only: [:create, :destroy]
    end
    resources :itineraries
    resources :employees
  end



  namespace :api do
    namespace :v1 do
      resources :users, only: [:create, :update, :show]
      resource :sessions, only: [:create]
      resource :password_restore, only: [:create]
      # resource :member do
      #   get :me, on: :collection
      #   get :index, on: :collection
      # end
    end
  end


  # root new_user_session_path #''"devise/sessions#new" #new_user_session_path#'inspection#new'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
