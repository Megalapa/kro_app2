class AddForegnkeyToMany < ActiveRecord::Migration[5.0]
  def change
    add_belongs_to :build_elements, :facility, foreign_key: true, index: true
    add_belongs_to :checklists, :inspection, foreign_key: true, index: true
    add_belongs_to :employees, :facility, foreign_key: true, index: true
    add_belongs_to :inventories, :employee, foreign_key: true, index: true
    # add_belongs_to :sectors, :employee, foreign_key: true, index: true
  end
end
