class AddForegnkeyToMany3 < ActiveRecord::Migration[5.0]
  def change
  	add_belongs_to :itineraries, :facility, foreign_key: true, index: true
  	add_belongs_to :inventories, :facility, foreign_key: true, index: true
  	add_belongs_to :inspections, :facility, foreign_key: true, index: true
  	add_belongs_to :furnitures, :placement, foreign_key: true, index: true
  	add_belongs_to :glasses, :placement, foreign_key: true, index: true
  	add_belongs_to :surfaces, :placement, foreign_key: true, index: true
  	# add_belongs_to :sectors, :itinerary, foreign_key: true, index: true
  	# add_belongs_to :placements, :sector, foreign_key: true, index: true
  end
end
