class EditEmploees < ActiveRecord::Migration[5.0]
  def change
    add_column :employees, :manager_id, :integer
    add_reference :employees, :employee, foreign_key: true
    add_column :employees, :name, :string
    add_column :employees, :surname, :string
    add_column :employees, :birthday, :datetime
    add_column :employees, :sex, :bool
    add_column :employees, :document_type, :string
    add_column :employees, :document_number, :string
  end
end
