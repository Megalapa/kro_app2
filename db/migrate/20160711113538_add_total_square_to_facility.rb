class AddTotalSquareToFacility < ActiveRecord::Migration[5.0]
  def change
    add_column :facilities, :total_square, :integer
  end
end
