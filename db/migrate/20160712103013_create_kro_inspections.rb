class CreateKroInspections < ActiveRecord::Migration[5.0]
  def change
    create_table :kro_inspections do |t|
      t.belongs_to :facility, foreign_key: true, index: true
      t.timestamps
    end
  end
end
