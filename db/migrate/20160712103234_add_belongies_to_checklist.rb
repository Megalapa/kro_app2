class AddBelongiesToChecklist < ActiveRecord::Migration[5.0]
  def change
    add_belongs_to :checklists, :kro_inspection, foreign_key: true, index: true
  end
end
