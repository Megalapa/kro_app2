class AddColumnsToFacility < ActiveRecord::Migration[5.0]
  def change

    change_table :facilities do |c|
      c.string :address
      c.string :representative
      c.string :contacts
      c.string :service_organization
    end
      
      
  end
end
