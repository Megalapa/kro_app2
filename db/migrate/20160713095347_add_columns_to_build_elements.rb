class AddColumnsToBuildElements < ActiveRecord::Migration[5.0]
  def change
    change_table :build_elements do |c|
      c.string :kind
      c.string :name
      c.string :address
      c.integer :total_square
      c.string :kind_of_access
    end
  end
end
