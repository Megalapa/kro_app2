class AddColumnsToPlacement < ActiveRecord::Migration[5.0]
  def change
    change_table :placements do |c|
      c.string :place_number
      c.string :description
      c.integer :placement_id
      c.string :name
      c.string :kind
      c.integer :square
    end
  end
end
