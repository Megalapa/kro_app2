class AddColumnsToSurface < ActiveRecord::Migration[5.0]
  def change
    add_column :surfaces, :kind, :string
    add_column :surfaces, :material, :string
  end
end
