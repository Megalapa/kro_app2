class AddColumnsToGlass < ActiveRecord::Migration[5.0]
  def change
    add_column :glasses, :kind, :string
    add_column :glasses, :square, :integer
  end
end
