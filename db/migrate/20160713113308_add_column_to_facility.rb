class AddColumnToFacility < ActiveRecord::Migration[5.0]
  def change
    add_column :facilities, :documents_list, :string
  end
end
