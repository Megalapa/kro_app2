class AddArrayColumnToFacility < ActiveRecord::Migration[5.0]
  def change
    add_column :facilities, :documents_list_array, :text, array: :true, default: []
  end
end
