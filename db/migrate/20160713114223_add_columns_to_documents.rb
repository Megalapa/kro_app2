class AddColumnsToDocuments < ActiveRecord::Migration[5.0]
  def change
    add_column :documents, :name, :string
    add_column :documents, :number, :string
    add_column :documents, :kind, :string
  end
end
