class AddColumnsToInventory < ActiveRecord::Migration[5.0]
  def change
    change_table :inventories do |c|
      c.string :kind
      c.string :identification_number
    end
  end
end
