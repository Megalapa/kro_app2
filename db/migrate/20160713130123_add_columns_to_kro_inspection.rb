class AddColumnsToKroInspection < ActiveRecord::Migration[5.0]
  def change
    change_table :kro_inspections do |c|
      c.datetime :starts
      c.datetime :ends
      c.integer :total_points
      c.integer :mark
      c.string :ispector_signature
      c.string :manager_signature
      c.string :representive_signature
      c.text :description
    end    
  end
end
