class AddArrayColumnToKroInpsection < ActiveRecord::Migration[5.0]
  def change
    add_column :kro_inspections, :inspect_mask, :text, array: :true, default:[]
  end
end
