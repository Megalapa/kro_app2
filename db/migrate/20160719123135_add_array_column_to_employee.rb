class AddArrayColumnToEmployee < ActiveRecord::Migration[5.0]
  def change
    add_column :employees, :languages, :text, array: :true, default:[]
  end
end
