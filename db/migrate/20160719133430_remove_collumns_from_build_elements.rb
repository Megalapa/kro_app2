class RemoveCollumnsFromBuildElements < ActiveRecord::Migration[5.0]
  def change
    remove_column :build_elements, :name, :string
    remove_column :build_elements, :total_square, :integer
  end
end
