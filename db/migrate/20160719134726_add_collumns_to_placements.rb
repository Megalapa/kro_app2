class AddCollumnsToPlacements < ActiveRecord::Migration[5.0]
  def change
    add_belongs_to :placements, :build_element, foreign_key: true, index: true
    add_column :placements, :wall, :string
    add_column :placements, :floor, :string
    add_column :placements, :ceiling, :string
  end
end
