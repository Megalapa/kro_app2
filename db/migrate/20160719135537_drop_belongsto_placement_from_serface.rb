class DropBelongstoPlacementFromSerface < ActiveRecord::Migration[5.0]
  def change
    remove_column :surfaces, :placement_id, :index
  end
end
