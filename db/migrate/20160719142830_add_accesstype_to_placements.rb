class AddAccesstypeToPlacements < ActiveRecord::Migration[5.0]
  def change
    add_column :placements, :access_kind, :string, default: "открытый"
  end
end
