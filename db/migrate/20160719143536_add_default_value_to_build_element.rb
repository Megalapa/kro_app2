class AddDefaultValueToBuildElement < ActiveRecord::Migration[5.0]
  def change
    change_column :build_elements, :kind_of_access, :string, default: "открытый"
  end
end
