class AddColumnsToKroInspectionArrays < ActiveRecord::Migration[5.0]
  def change
    add_column :kro_inspections, :placements, :text, array: :true, default:[]
    add_column :kro_inspections, :nested_placements, :text, array: :true, default:[]
    add_column :kro_inspections, :furnitures, :text, array: :true, default:[]
    add_column :kro_inspections, :glass, :text, array: :true, default:[]
  end
end
