class CreateTimeSheets < ActiveRecord::Migration[5.0]
  def change
    create_table :time_sheets do |t|
      t.string :timestamp
      t.string :kind_of_enterence
      t.string :card_id
      t.string :gate_id

      t.timestamps
    end
  end
end
