class RemoveCollumnsFromTimeSheet < ActiveRecord::Migration[5.0]
  def change
    remove_column :time_sheets, :created_at, :datetime
    remove_column :time_sheets, :updated_at, :datetime
  end
end
