class CreateShipments < ActiveRecord::Migration[5.0]
  def change
    create_table :shipments do |t|
      t.belongs_to :facility, foreign_key: true

      t.timestamps
    end
  end
end
