class CreateSupplies < ActiveRecord::Migration[5.0]
  def change
    create_table :supplies do |t|
      t.belongs_to :shipment, foreign_key: true
      t.string :kind
      t.string :amount
      t.string :description
      t.string :name

      t.timestamps
    end
  end
end
