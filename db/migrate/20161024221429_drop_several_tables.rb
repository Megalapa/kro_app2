class DropSeveralTables < ActiveRecord::Migration[5.0]
  def change
    drop_table :ispection_scheduls
    drop_table :checklists
    drop_table :inspections
    drop_table :surfaces
    add_column :facilities, :inspections_amount, :integer
  end
end
