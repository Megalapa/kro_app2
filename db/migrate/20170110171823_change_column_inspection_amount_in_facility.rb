class ChangeColumnInspectionAmountInFacility < ActiveRecord::Migration[5.0]
  def change
    change_column :facilities, :inspections_amount, :integer, default: 0
  end
end
