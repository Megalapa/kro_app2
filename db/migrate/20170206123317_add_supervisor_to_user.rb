class AddSupervisorToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :supervisor, :boolean, default: false
    add_column :users, :inspector, :boolean, default: false
  end
end
