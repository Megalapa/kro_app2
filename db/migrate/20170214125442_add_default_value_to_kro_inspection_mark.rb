class AddDefaultValueToKroInspectionMark < ActiveRecord::Migration[5.0]
  def change
    
    reversible do |change|
      change.up do
        change_column :kro_inspections, :mark, :float, default: 0
        change_column :kro_inspections, :total_points, :integer, default: 0
      end
      change.down do
        change_column :kro_inspections, :mark, :float, default: nil
        change_column :kro_inspections, :total_points, :integer, default: nil
      end
    end
  end
end
