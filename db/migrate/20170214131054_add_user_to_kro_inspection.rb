class AddUserToKroInspection < ActiveRecord::Migration[5.0]
  def change
    add_belongs_to :kro_inspections, :user, foreign_key: true, index: true
  end
end
