class CreateInsideObjects < ActiveRecord::Migration[5.0]
  def change
    create_table :inside_objects do |t|
      t.belongs_to  :placement, index: true, foreign_key: true
      t.string      :material
      t.string      :kind
      t.timestamps
    end
  end
end
