class CreateExaminations < ActiveRecord::Migration[5.0]
  def change
    create_table :examinations do |t|
      
      t.belongs_to  :user,               index: true, foreign_key: true
      t.belongs_to  :facility,           index: true, foreign_key: true
      t.integer     :total_points,       default: 0
      t.float       :mark,               default: 0.0
      t.text        :comments
      t.text        :placements,         default: [], array: true
      t.text        :nested_placements,  default: [], array: true
      t.text        :inside_objects,     default: [], array: true, comments: "for furniture in placement"

      t.timestamps
    end
  end
end
