class AddCollumnsToItinerary < ActiveRecord::Migration[5.0]
  def change
    add_column :itineraries, :name, :string
    add_column :itineraries, :build_elements, :text, array: :true, default:[]
    add_column :itineraries, :placements, :text, array: :true, default:[]
    add_column :itineraries, :nested_placements, :text, array: :true, default:[]
  end
end
