class CreateComments < ActiveRecord::Migration[5.0]
  def change
    create_table :comments do |t|
      t.belongs_to :parent, polymorphic: true
      t.belongs_to :examination, foreign_key: true
      t.text :content

      t.timestamps
    end
    add_index :comments, [:examination_id, :parent_id, :parent_type]
  end

end
