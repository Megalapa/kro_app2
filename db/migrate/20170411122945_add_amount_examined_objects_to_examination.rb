class AddAmountExaminedObjectsToExamination < ActiveRecord::Migration[5.0]
  def change
    add_column :examinations, :amount_examined_objects, :integer, comment: "to store amount of objects wich was examined"
  end
end
