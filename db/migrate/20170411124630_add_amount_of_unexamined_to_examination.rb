class AddAmountOfUnexaminedToExamination < ActiveRecord::Migration[5.0]
  def change
    add_column :examinations, :unexamined, :text, array: :true, default:[]
  end
end
