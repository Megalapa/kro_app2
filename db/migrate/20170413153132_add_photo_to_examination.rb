class AddPhotoToExamination < ActiveRecord::Migration[5.0]
  def change
    add_column :examinations, :photo, :string
  end
end
