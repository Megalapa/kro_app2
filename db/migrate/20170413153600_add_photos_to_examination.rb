class AddPhotosToExamination < ActiveRecord::Migration[5.0]
  def change
    add_column :examinations, :photos, :text, array: :true, default:[]
  end
end
