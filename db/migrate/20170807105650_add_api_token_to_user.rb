class AddApiTokenToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :token_for_api, :string
    add_column :users, :api_token_expires_at, :datetime
  end
end
