# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170807105650) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "attachments", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "file"
    t.string   "attachable_type"
    t.integer  "attachable_id"
    t.string   "attachable_for_type"
    t.integer  "attachable_for_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.index ["attachable_for_type", "attachable_for_id"], name: "index_attachments_on_attachable_for_type_and_attachable_for_id", using: :btree
    t.index ["attachable_type", "attachable_id"], name: "index_attachments_on_attachable_type_and_attachable_id", using: :btree
    t.index ["user_id"], name: "index_attachments_on_user_id", using: :btree
  end

  create_table "build_elements", force: :cascade do |t|
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "facility_id"
    t.string   "kind"
    t.string   "address"
    t.string   "kind_of_access", default: "открытый"
    t.index ["facility_id"], name: "index_build_elements_on_facility_id", using: :btree
  end

  create_table "comments", force: :cascade do |t|
    t.string   "parent_type"
    t.integer  "parent_id"
    t.integer  "examination_id"
    t.text     "content"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["examination_id", "parent_id", "parent_type"], name: "index_comments_on_examination_id_and_parent_id_and_parent_type", using: :btree
    t.index ["examination_id"], name: "index_comments_on_examination_id", using: :btree
    t.index ["parent_type", "parent_id"], name: "index_comments_on_parent_type_and_parent_id", using: :btree
  end

  create_table "documents", force: :cascade do |t|
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "facility_id"
    t.string   "name"
    t.string   "number"
    t.string   "kind"
    t.index ["facility_id"], name: "index_documents_on_facility_id", using: :btree
  end

  create_table "employees", force: :cascade do |t|
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "facility_id"
    t.integer  "manager_id"
    t.integer  "employee_id"
    t.string   "name"
    t.string   "surname"
    t.datetime "birthday"
    t.boolean  "sex"
    t.string   "document_type"
    t.string   "document_number"
    t.text     "languages",       default: [],              array: true
    t.index ["employee_id"], name: "index_employees_on_employee_id", using: :btree
    t.index ["facility_id"], name: "index_employees_on_facility_id", using: :btree
  end

  create_table "examinations", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "facility_id"
    t.integer  "total_points",            default: 0
    t.float    "mark",                    default: 0.0
    t.text     "comments"
    t.text     "placements",              default: [],                                                                        array: true
    t.text     "nested_placements",       default: [],                                                                        array: true
    t.text     "inside_objects",          default: [],                                                                        array: true
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.integer  "amount_examined_objects",                            comment: "to store amount of objects wich was examined"
    t.text     "unexamined",              default: [],                                                                        array: true
    t.string   "photo"
    t.text     "photos",                  default: [],                                                                        array: true
    t.index ["facility_id"], name: "index_examinations_on_facility_id", using: :btree
    t.index ["user_id"], name: "index_examinations_on_user_id", using: :btree
  end

  create_table "facilities", force: :cascade do |t|
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "name"
    t.integer  "total_square"
    t.string   "address"
    t.string   "representative"
    t.string   "contacts"
    t.string   "service_organization"
    t.string   "documents_list"
    t.text     "documents_list_array", default: [],              array: true
    t.integer  "inspections_amount",   default: 0
  end

  create_table "furnitures", force: :cascade do |t|
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "placement_id"
    t.string   "material"
    t.string   "kind"
    t.index ["placement_id"], name: "index_furnitures_on_placement_id", using: :btree
  end

  create_table "glasses", force: :cascade do |t|
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "placement_id"
    t.string   "kind"
    t.integer  "square"
    t.index ["placement_id"], name: "index_glasses_on_placement_id", using: :btree
  end

  create_table "inside_objects", force: :cascade do |t|
    t.integer  "placement_id"
    t.string   "material"
    t.string   "kind"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["placement_id"], name: "index_inside_objects_on_placement_id", using: :btree
  end

  create_table "inventories", force: :cascade do |t|
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.integer  "employee_id"
    t.integer  "facility_id"
    t.string   "kind"
    t.string   "identification_number"
    t.index ["employee_id"], name: "index_inventories_on_employee_id", using: :btree
    t.index ["facility_id"], name: "index_inventories_on_facility_id", using: :btree
  end

  create_table "itineraries", force: :cascade do |t|
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "facility_id"
    t.string   "name"
    t.text     "build_elements",    default: [],              array: true
    t.text     "placements",        default: [],              array: true
    t.text     "nested_placements", default: [],              array: true
    t.index ["facility_id"], name: "index_itineraries_on_facility_id", using: :btree
  end

  create_table "kro_inspections", force: :cascade do |t|
    t.integer  "facility_id"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.datetime "starts"
    t.datetime "ends"
    t.integer  "total_points",           default: 0
    t.float    "mark",                   default: 0.0
    t.string   "ispector_signature"
    t.string   "manager_signature"
    t.string   "representive_signature"
    t.text     "description"
    t.text     "inspect_mask",           default: [],               array: true
    t.text     "placements",             default: [],               array: true
    t.text     "nested_placements",      default: [],               array: true
    t.text     "furnitures",             default: [],               array: true
    t.text     "glass",                  default: [],               array: true
    t.integer  "user_id"
    t.index ["facility_id"], name: "index_kro_inspections_on_facility_id", using: :btree
    t.index ["user_id"], name: "index_kro_inspections_on_user_id", using: :btree
  end

  create_table "placements", force: :cascade do |t|
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.string   "place_number"
    t.string   "description"
    t.integer  "placement_id"
    t.string   "name"
    t.string   "kind"
    t.integer  "square"
    t.integer  "build_element_id"
    t.string   "wall"
    t.string   "floor"
    t.string   "ceiling"
    t.string   "access_kind",      default: "открытый"
    t.index ["build_element_id"], name: "index_placements_on_build_element_id", using: :btree
  end

  create_table "shipments", force: :cascade do |t|
    t.integer  "facility_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["facility_id"], name: "index_shipments_on_facility_id", using: :btree
  end

  create_table "supplies", force: :cascade do |t|
    t.integer  "shipment_id"
    t.string   "kind"
    t.string   "amount"
    t.string   "description"
    t.string   "name"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["shipment_id"], name: "index_supplies_on_shipment_id", using: :btree
  end

  create_table "time_sheets", force: :cascade do |t|
    t.string "timestamp"
    t.string "kind_of_enterence"
    t.string "card_id"
    t.string "gate_id"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.boolean  "supervisor",             default: false
    t.boolean  "inspector",              default: false
    t.string   "name"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.string   "token_for_api"
    t.datetime "api_token_expires_at"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  add_foreign_key "attachments", "users"
  add_foreign_key "build_elements", "facilities"
  add_foreign_key "comments", "examinations"
  add_foreign_key "documents", "facilities"
  add_foreign_key "employees", "employees"
  add_foreign_key "employees", "facilities"
  add_foreign_key "examinations", "facilities"
  add_foreign_key "examinations", "users"
  add_foreign_key "furnitures", "placements"
  add_foreign_key "glasses", "placements"
  add_foreign_key "inside_objects", "placements"
  add_foreign_key "inventories", "employees"
  add_foreign_key "inventories", "facilities"
  add_foreign_key "itineraries", "facilities"
  add_foreign_key "kro_inspections", "facilities"
  add_foreign_key "kro_inspections", "users"
  add_foreign_key "placements", "build_elements"
  add_foreign_key "shipments", "facilities"
  add_foreign_key "supplies", "shipments"
end
