# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
facility = Facility.create(name: 'new_facility')
build_element = facility.build_elements.create()
sector = build_element.sectors.create()
placement = sector.placements.create()
nested_placement = placement.nested_placements.create()

