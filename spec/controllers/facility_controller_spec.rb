RSpec.describe FacilityController, :type => :controller do
  
  let(:facility) { create(:facility) }

  describe 'GET #index' do
  	before { get :index }
    let(:facilities) { create_list(:facility, 3) }

  	it 'returns array of facilities' do
  	  expect(assigns(:this_all)).to match_array facilities
  	end
  	it 'renders template' do
  	  expect(response).to render_template :index
  	end
  end
  describe 'GET #new' do
    before { get :new }
    it 'assigns current facility' do
      expect(assigns(:this_new)).to be_a_new(Facility)
    end
  end
  describe 'POST #create' do

    let(:request){ post :create, facility: attributes_for(:facility) }
    context 'with normal params' do

      it 'creates new facility' do
        expect{ request }.to change(Facility, :count).by(1)
      end
    end
    context 'with extra prams' do
      before { post :create, facility: attributes_for(:with_extra_attributes) }

      it 'redirects' do
        expect(response).to redirect_to facility_index_path
      end

      it 'shouldnt permit extra attributes' do
        # expect{ post :create, facility: attributes_for(:with_extra_attributes) }
             # .to change(facility.reload, :name).to('permit params')
        # expect(facility.total_square).to eq nil
        # expect(facility.name).to eq 'permit params'
      end

      it 'with illegal values' do
        expect{ post :create, facility: attributes_for(:with_illegal_values) }
             .to_not change(Facility, :count)
      end

    end
  end
  describe 'GET #show' do
    before { get :show, params: { id: facility }}
    it 'assigns current facility' do
      expect(assigns(:this)).to eq facility
    end
  end
  describe 'GET #edit' do
    before { get :edit, id: facility }
    it 'assigns current facility' do
      expect(assigns(:this)).to eq facility
    end
  end
  describe 'POST #update' do
    let(:request) { patch :update, id: facility, facility: {name: 'new_name'} }
    
    it 'updates model' do
      request
      facility.reload
      expect(facility.name).to eq 'new_name'
    end
  end
  
end
