FactoryBot.define do
  factory :facility do
    name 'name'
  end
  factory :changed_facility do
    name 'new_name'
  end
  factory :facilities do
    name 'name'
  end

  factory :with_extra_attributes, class: Facility do
    name 'permit params'
    total_square 6
  end

  factory :with_illegal_values, class: Facility do
    name nil
    total_square nil
  end
end
