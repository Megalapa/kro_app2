FactoryBot.define do
  factory :supply do
    shipment nil
    kind "MyString"
    amount "MyString"
    description "MyString"
    name "MyString"
  end
end
