FactoryBot.define do
  factory :user do
    sequence :email do |n|
      "person#{n}@example.com"
    end
    password "hi,hello"
    name "my name is what?"
    confirmed_at Time.now - 2.days

    trait :supervisor do
      supervisor true
    end

    trait :inspector do
      inspector true
    end
    
  end
end
