require 'rails_helper'

RSpec.describe BuildElement, :type => :model do
  it { should belong_to(:facility) }
  # it { should have_many(:sectors).dependent(:destroy) }
  it { should have_many(:placements).dependent(:destroy) }
  # it { should have_many(:sectors).through(:sectors) }
  it { should accept_nested_attributes_for(:placements) }
end
