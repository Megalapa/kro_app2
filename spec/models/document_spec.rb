require 'rails_helper'

RSpec.describe Document, :type => :model do
  it { should belong_to(:facility).dependent(:destroy) }
end
