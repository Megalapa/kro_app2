RSpec.describe Employee, :type => :model do
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:surname) }
  it { should validate_presence_of(:birthday) }
  it { should validate_presence_of(:sex) }
  it { should validate_presence_of(:document_type) }
  it { should validate_presence_of(:document_number) }
  it { should belong_to(:facility) }
  it { should have_many(:inventory) }
  it { should have_many(:sectors) }
end
