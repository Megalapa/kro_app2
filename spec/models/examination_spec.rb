require 'rails_helper'

RSpec.describe Examination, type: :model do
  context "#total_available_count" do
    let(:placement_1){ create(:placement, id: 23 ) }
    let(:placement_2){ create(:placement, id: 45 ) }
    let(:placement_3){ create(:placement, id: 6 ) }
    let(:examination){ create(:examination, placements: [23,45,6]) }
    
    it "return total_points" do
      expect(examination.instance_eval{total_available_count}).to eq "3"
    end
  end
  # context "after_create it's" do
  #   let(:facility){ create(:facility) }
  #   it "count and return total_points" do
  #     examination = facility.examinations.create(placements: [23,45,6])
  #     expect(examination.total_points).to eq "3"
  #   end
  # end
end
