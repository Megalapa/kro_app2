require 'rails_helper'

RSpec.describe Facility, :type => :model do
  it { should have_many(:documents).dependent(:destroy)}
  it { should have_many(:build_elements).dependent(:destroy) }
  it { should have_many(:itinerarys).dependent(:destroy) }
  it { should have_many(:employees) }
  it { should have_many(:inventory) }
  it { should have_many(:kro_inspections).dependent(:destroy) }
  
  it { should accept_nested_attributes_for(:build_elements) }

  it { should validate_presence_of(:name) }
end
