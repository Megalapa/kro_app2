require 'rails_helper'

RSpec.describe Glass, :type => :model do
  it { should belong_to(:placement).dependent(:destroy) }
end
