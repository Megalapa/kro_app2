require 'rails_helper'

RSpec.describe Inventory, :type => :model do
  it { should belong_to(:facility) }
  it { should belong_to(:employee) }
end
