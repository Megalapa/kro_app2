require 'rails_helper'

RSpec.describe Itinerary, :type => :model do
  it { should belong_to(:facility).dependent(:destroy) }
  it { should have_many(:sectors) }
end
