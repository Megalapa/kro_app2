require 'rails_helper'

RSpec.describe KroInspection, :type => :model do
  it { should belong_to(:facility) }
  it { should have_many(:checklists).dependent(:destroy) }
end
