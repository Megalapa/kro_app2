require 'rails_helper'

RSpec.describe Placement, :type => :model do
  it { should belong_to(:build_element) }
  it { should have_many(:furniture).dependent(:destroy) }
  # it { should have_many(:surface).dependent(:destroy) }
  it { should have_many(:glass).dependent(:destroy) }
end
