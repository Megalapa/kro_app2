require 'rails_helper'

RSpec.describe Supply, type: :model do
  it { should belong_to :shipment }
  it { should validate_presence_of :shipment_id }
end
