describe FacilityPolicy do
  subject { described_class }
  
  permissions :index?, :create?, :update?, :destroy?, :dublicating? do
    it "denies access if user is not supervisor" do
      expect(subject).not_to permit(User.new(supervisor: false), Facility.new())
    end
  end

  permissions :show? do
    it "access anyway" do
      expect(subject).to permit(User.new(supervisor: false), Facility.new())
    end
  end
end