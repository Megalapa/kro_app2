describe KroInspectionPolicy do
  subject { described_class }

  # permissions :create? do
  #   it "denies access if user is not supervisor" do
  #     expect(subject).not_to permit(User.new(supervisor: false), KroInspection.new())
  #   end
  # end

  permissions :show? do
    it "access anyway" do
      # expect(subject).to permit(User.new(supervisor: false), KroInspection.new())
      is_expected.to permit(User.new(supervisor: false), KroInspection.new())
    end
  end
end
