describe UserPolicy do
  subject { described_class }
  
  permissions :index? do
    let(:user){create(:user)}
    let(:supervisor){create(:user, :supervisor)}
    let(:inspector){create(:user, :inspector)}

    it "denies access if user is not supervisor" do
      is_expected.to_not permit(user, User)
    end

    it "allow to access if user supervisor" do
      is_expected.to permit(supervisor, User)
    end

    it "allow to access if user inspector" do
      is_expected.to permit(supervisor, User)
    end
  end

  # permissions :show?, :update? do
  #   it "access anyway" do
  #     expect(subject).to permit(User.new(supervisor: false))
  #   end
  # end
end